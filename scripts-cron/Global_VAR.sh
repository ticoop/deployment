#!/bin/bash

SCRIPTSCRON_USER_NAME="cron-scripts"
SCRIPTSCRON_USER_HOMEROOT="/data"
SCRIPTSCRON_USER_SUBLOGFOLDER="crontab"

SCRIPTSCRON_REPO_NAME='scripts-cron'
SCRIPTSCRON_REPO_GITURL='git@gitlab.com:ticoop/scripts-cron.git'
SCRIPTSCRON_REPO_REQUIREMENTSFILE="requirements.txt"

SCRIPTSCRON_CRONFILE="scripts.crontab"