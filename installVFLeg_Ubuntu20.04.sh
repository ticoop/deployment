#!/bin/bash

# Version 1.0 - 2021-10-09 - Maxime Collin - Creation du script
#
# Ce script va déployer l'application VFLeg sur une machine. L'objectif est d'avoir une configuration pour une machine utilisateur dédiée :
# - Création d'un utilisateur VFLeg (non admin)
# - Connexion automatique de l'utilisateur
# - Lancement automatique de l'application
# - Ne fait rien d'autre
# 
# Ne touche pas au reste de la configuration de la machine, s'il y a d'autres applis, utilisateurs, ... tout reste en place
# Il a besoin des droits admin
# 
# Il va commencer par vérifier que les paquets RPM et Python nécessaires au déployement sont installés 
# Et créé un lien symbolique pour python3 pour l'appeler avec python
# Il créé un groupe et un répertoire pour logguer les scripts si ce n'est pas déjà fait
# Il créé un user pour lancer l'appli et déploi les sources dans son homedir avec git, puis installe les dépendances
# Et réalise la configuration du user pour la connexion automatique et le lancement de l'appli au démarrage
# 

source ./Util/util.sh
source ./VFLeg/Global_VAR.sh

# ------------ RPM Packages needed
echo "Install RPM Packages"
installRPMpackage $UTIL_RPMPACKAGES_NEEDED $VFLEG_RPMPACKAGES_NEEDED

# ------------ Python 3 Symlink
echo "Create Symlink for Python3"
createPython3Symlink /usr/bin/python

# ------------ Python Packages needed for install process
echo "Install Python Packages"
installPipPackage --sudoer $UTIL_PIPPACKAGES_NEEDED

# ------------ Put Custom Package in Python PATH
echo "Export Python PATH"
allowENVvarPYTHONPATHforSudoCmd
export PYTHONPATH="${PYTHONPATH}:${PWD}/Util"

# ------------ Create Log Group
echo "Create Log Group and Log Root Folder"
sudo python Util/createLogGroup.py
RETURN_CODE=$?

# If something going wrong -> terminated
if [ $RETURN_CODE != 0 ]
then
    sendErrorMsg "Le groupe n'a pas pu être créé"
    sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
    exit $RETURN_CODE
fi

# ------------ Create User
echo "Create User VFLeg"
sudo python Util/createUser.py --user_name=$VFLEG_USER_NAME --autoLogin --group=$VFLEG_USER_GROUPS
RETURN_CODE=$?

# If something going wrong -> terminated
if [ $RETURN_CODE != 0 ]
then
    sendErrorMsg "le user n'a pas pu être créé"
    sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
    exit $RETURN_CODE
fi

# ------------ Deploying Source
echo "Deploying source"
export VFLEG_REPO_PATH="${VFLEG_USER_HOMEROOT}/${VFLEG_USER_NAME}/${VFLEG_REPO_NAME}"
sudo su -m $VFLEG_USER_NAME -c 'python -c "from Util import deployGit; deployGit.DeployGit('"'${VFLEG_REPO_GITURL}'"')"'

RETURN_CODE=$?

# If something going wrong -> terminated
if [ $RETURN_CODE != 0 ]
then
    sendErrorMsg "les sources n'ont pas pu être déployées"
    sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
    exit $RETURN_CODE
fi

# ------------ Python Packages needed for install process
echo "Install Python Packages"
export SOURCE_REQUIREMENTSFILE="${VFLEG_REPO_PATH}/${VFLEG_REPO_REQUIREMENTSFILE}"
echo "Saisissez le mot de passe de l'utilisateur ${VFLEG_USER_NAME}"
su $VFLEG_USER_NAME -c "source Util/util.sh; installPipPackage $SOURCE_REQUIREMENTSFILE"
RETURN_CODE=$?

# If something going wrong -> terminated
if [ $RETURN_CODE != 0 ]
then
    sendErrorMsg "les dépendances des sources n'ont pas pu être installées"
    sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
    exit $RETURN_CODE
fi

# ------------ VFLeg cache dir
echo "Création du répertoire de cache de VFLeg"
sudo su -m $VFLEG_USER_NAME -c 'mkdir -p '${VFLEG_REPO_PATH}'/data/cache/images'

# ------------ VFLeg Launcher
echo "Création du lanceur de l'application VFLeg"
sudo su -m $VFLEG_USER_NAME -c 'mkdir -p '${DESKTOP_DIR}' && cp VFLeg/'${VFLEG_DESKTOP_FILE}' '${DESKTOP_DIR}

# ------------ VFLeg autostart
echo "Lancement de VFLeg au démarrage"
sudo su -m $VFLEG_USER_NAME -c 'mkdir -p '${AUTOSTART_DIR}' && ln -s '${DESKTOP_DIR}'/'${VFLEG_DESKTOP_FILE}' '${AUTOSTART_DIR}'/'${VFLEG_DESKTOP_FILE}

# ------------ VFLeg in dock
echo "Remplacement des applications dans le Dock par VFLeg"
# /bin/sh -c -> for launch a sub shell
# export $(dbus-launch) && export DISPLAY=:0.0 -> dconf need dbus, so we have to set some ENV-VAR but the -m option of su cause a bug with dconf (he seems doesn't like some ENV-VAR) 
sudo su $VFLEG_USER_NAME -c '/bin/sh -c "export $(dbus-launch) && export DISPLAY=:0.0 && dconf write /org/gnome/shell/favorite-apps \"['\'${VFLEG_DESKTOP_FILE}\'']\""'

# ------------ Desactivate gnome-initial-setup for VFLeg user
echo "Désactivation de l'initialisation des comptes en ligne"
sudo su -m $VFLEG_USER_NAME -c "echo 'yes' > ${VFLEG_USER_HOMEROOT}/${VFLEG_USER_NAME}/.config/gnome-initial-setup-done"


# ------------ Desactivate lock screen
echo "Désactivation de l'écran de vérouillage"
# /bin/sh -c -> for launch a sub shell
# export $(dbus-launch) && export DISPLAY=:0.0 -> dconf need dbus, so we have to set some ENV-VAR but the -m option of su cause a bug with dconf (he seems doesn't like some ENV-VAR) 
sudo su $VFLEG_USER_NAME -c '/bin/sh -c "export $(dbus-launch) && export DISPLAY=:0.0 && gsettings set org.gnome.desktop.screensaver lock-enabled false"'
sudo su $VFLEG_USER_NAME -c '/bin/sh -c "export $(dbus-launch) && export DISPLAY=:0.0 && gsettings set org.gnome.desktop.session idle-delay 0"'

# ------------ Open write perm on scale (/dev/ttyUSB0) to others
echo "Ouverture des droits en écriture sur la balance pour tout le monde"
sudo chmod 666 /dev/ttyUSB0

# ------------ Conf printer
echo "Saisissez le nom de l'étiquetteuse"
read SCALE_NAME
echo "Configuration de l'étiquetteuse"
lpadmin -p $SCALE_NAME -m raw