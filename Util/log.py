# Version 1.0 - 2020-04-19 - Cyril CARGOU - Lib de fonctions génériques et/ou preconfigurée
# Version 2.0 - 2021-10-11 - Maxime Collin - Séparation des fonctions de logs du reste
#
# Cette lib fournit une classe Log() avec des fonctions préconfigurées afin de 
#   - Créer un fichier de log unique par script
#   - Uniformiser le format des fichiers (nom, forme des logs)
#   - Avoir des messages de fin de script standard 
# 
# Elle s'utilise de cette façon : 
#   from Util import log
#   logger = log.Log()
#   logger.logInfo("Hello World")
#   logger.logError("Mauvais nombre d'arguments")
#   logger.logCloseAndExitOnError("Ce script a besoin de 2 arguments")
# 
# Ceci va créer un fichier YYYY-MM-DD_HH:MM:SS_scriptname.log
# dans le répertoire ROOTFOLDER ou dans un des sous-répertoires si définit lors de l'appel à logOpen()
# Le contenu du fichier sera : 
# 
#   2021-10-11 02:00:02,261 ; INFO ; ---------------------------------
#   2021-10-11 02:00:02,261 ; INFO ; Lancement du script : /path/to/script
#   2021-10-11 02:00:02,263 ; INFO ; Hello World
#   2021-10-11 02:00:05,305 ; ERROR ; Mauvais nombre d'arguments
#   2021-10-11 02:00:05,305 ; ERROR ; Fin du script : ERREUR -> Ce script a besoin de 2 arguments
# 
# 
# LES FONCTIONS DISPONIBLES :
#   Log(subFolder) (constructeur) créé le fichier de log et écrit les 2 premières lignes. Elle doit être appelée en 1ère
# 
#   logInfo(msg)       Envoi le message en paramètre avec un LEVEL_INFO
#   logDebug(msg)      Envoi le message en paramètre avec un LEVEL_DEBUG
#   logWarning(msg)    Envoi le message en paramètre avec un LEVEL_WARNING
#   logError(msg)      Envoi le message en paramètre avec un LEVEL_ERROR
# 
#   logClose()                  Envoi le message de fin de script : OK
#   logCloseAndExit()           Envoi le message de fin de script : OK et termine le script avec le code retour 0
#   logCloseOnError(msg)        Envoi le message de fin de script : NOK
#   logCloseAndExitOnError(msg) Envoi le message de fin de script : NOK et termine le script avec le code retour 1
# 


#############################################################################################################################
####################################################### IMPORT MODULES ######################################################
#############################################################################################################################

import os               # for check path
import logging          # for allow all modules to log in the same file
import datetime         # for track command datetime 
import grp, pwd         # for check user permission on folder
from Util import util   # A custom lib for get script name and path


class Log:

    #########################################################################################################################
    #################################################### DEFINE CONSTANTS ###################################################
    #########################################################################################################################

    ROOTFOLDER = '/var/log/ticoop'
    LOGGROUP = 'logTiCoop'


    #########################################################################################################################
    #################################################### DEFINE FUNCTIONS ###################################################
    #########################################################################################################################

    # ------------------------------------------------------------------------------------------------------
    # ---------------------------------------------- __init__ ----------------------------------------------
    # ------------------------------------------------------------------------------------------------------
    # 
    # DESCRIPTION : 
    #   Le constructeur créé un fichier de log et l'objet self.logger
    #   S'ils ne sont pas définis et y écrit le datetime du lancement du script
    # 
    #   Tous les fichiers sont stockés dans LOG_ROOTFOLDER.
    #   Il est possible d'indiquer un sous-répertoire en paramètre
    # 
    # PARAMÈTRES :
    #   (optionel) logSubFolder:str = Chemin relatif du sous-répertoire de log. Par défaut NULL
    #
    # RETURN :
    #   nothing
    # 
    # USAGE :
    #   Log()
    #   Log('/path/to/log/sub/folder')
    # 
    def __init__(self, logSubFolder = ''):

        # ------------- CHECK PARAMETERS -------------
        # If logSubFolder defined
        if logSubFolder != '':
            # Check if first character is a '/' or not
            if logSubFolder[0] != '/':
                # if not -> add it
                logSubFolder = '/' + logSubFolder


        # ------------ CHECK PERMISSIONS -------------
        self.logFolder = Log.ROOTFOLDER + logSubFolder
        if not self.__checkLogFolderAndPermission():
            print ("Fin du script")
            exit(1)

        # --------------------------------------- CREATE LOG FILE ------------------------------------------
        # Get script name for the log file's name
        scriptName = util.getScriptName()
        # Get the current datetime for the log file's name
        fileDate = datetime.datetime.today().strftime("%Y-%m-%d_%H%M%S")
        # Create the log file
        hdlr = logging.FileHandler(Log.ROOTFOLDER+logSubFolder + "/" + fileDate + "_" + scriptName + '.log')
        # Define his format (DateTime ; LOG_LEVEL ; MSG)
        hdlr.setFormatter(logging.Formatter('%(asctime)s ; %(levelname)s ; %(message)s'))

        # -------------- CREATE THE LOG OBJECT ---------------
        # Open the logger
        self.logger = logging.getLogger(scriptName)
        # Attach the log file 
        self.logger.addHandler(hdlr) 
        # Define the log level (can be INFO / WARNING / ERROR)
        self.logger.setLevel(logging.DEBUG)


        # ---------------------- LOG START SCRIPT ----------------------
        self.logInfo('---------------------------------')
        self.logInfo('Lancement du script :' + util.getScriptFullPath())


    # -----------------------------------------------------------------------------------------------------------------------
    # ------------------------------------------- __checkLogFolderAndPermission() -------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------------
    # 
    # DESCRIPTION : 
    #   (! Réservée à un usage interne à la lib ! )
    #   Cette fonction vérifie que le répertoire dans lequel le fichier de log sera écrit existe
    #   Et que l'utilisateur est membre du groupe LOGGROUP et a les droits en écriture sur le répertoire
    # 
    # PARAMÈTRES :
    #   Aucun
    #
    # RETURN :
    #   True if OK,
    #   False sinon
    # 
    # USAGE :
    #   checkLogFolderAndPermission()
    # 
    def __checkLogFolderAndPermission(self):

        # Check if logDir exist
        if not os.path.isdir(self.logFolder):
            print ("Erreur : le répertoire de log " + self.logFolder + " n'existe pas")
            return False

        # Check if user is member of logGroup
        username = pwd.getpwuid(os.getuid()).pw_name
        logGroup_members = grp.getgrnam(Log.LOGGROUP).gr_mem 
        if not username in logGroup_members:
            print ("Erreur : l'utilisateur " + username + " n'est pas membre du groupe " + Log.LOGGROUP)
            return False

        # Check ifuUser has write permission on logDir
        if not os.access(self.logFolder, os.W_OK):
            print ("Erreur : l'utilisateur " + username + " n'a pas les droits en écriture sur le dossier " + self.logFolder)
            return False

        return True


    # -------------------------------------
    # ------------ logInfo() --------------
    # -------------------------------------
    # 
    # DESCRIPTION : 
    #   Cette fonction envoi un message
    #   d'info dans les LOGS
    # 
    # PARAMÈTRES :
    #   msg:str = Un message d'info
    #
    # RETURN :
    #   Nothing
    # 
    # USAGE :
    #   logInfo("Mon message d'info")
    # 
    def logInfo(self, msg):
        return self.__log(msg,logging.INFO)


    # --------------------------------------
    # ------------ logDebug() --------------
    # --------------------------------------
    # 
    # DESCRIPTION : 
    #   Cette fonction envoi un message
    #   de debug dans les LOGS
    # 
    # PARAMÈTRES :
    #   msg:str = Un message de debug
    #
    # RETURN :
    #   Nothing
    # 
    # USAGE :
    #   logDebug("Mon message de debug")
    #
    def logDebug(self, msg):
        return self.__log(msg,logging.DEBUG)


    # ----------------------------------------
    # ------------ logWarning() --------------
    # ----------------------------------------
    # 
    # DESCRIPTION : 
    #   Cette fonction envoi un message
    #   de warning dans les LOGS
    # 
    # PARAMÈTRES :
    #   msg:str = Un message de warning
    #
    # RETURN :
    #   Nothing
    # 
    # USAGE :
    #   logWarning("Mon message de warning")
    #
    def logWarning(self, msg):
        return self.__log(msg,logging.WARNING)


    # --------------------------------------
    # ------------ logError() --------------
    # --------------------------------------
    # 
    # DESCRIPTION : 
    #   Cette fonction envoi un message
    #   d'erreur dans les LOGS
    # 
    # PARAMÈTRES :
    #   msg:str = Un message d'erreur
    #
    # RETURN :
    #   Nothing
    # 
    # USAGE :
    #   logError("Mon message d'erreur")
    #
    def logError(self, msg):
        return self.__log(msg,logging.ERROR)


    # -------------------------------------------------------------------
    # ----------------------------- __log() -----------------------------
    # -------------------------------------------------------------------
    # 
    # DESCRIPTION :
    #   (! Réservée à un usage interne de la lib !)
    #   Cette fonction envoi un message dans les LOGS
    #   avec le LOG_LEVEL spécifié
    # 
    # PARAMÈTRES :
    #   msg:str = Un message
    #   type:int = la lib logging définie des constantes INFO, DEBUG, ...
    #
    # RETURN :
    #   Nothing
    # 
    # USAGE :
    #   log("Mon message")
    #
    def __log(self, msg:str, type=logging.INFO):
        # Print the log in STDOUT
        print (msg)

        # Case on LOG_LEVEL -> put the log in LOGGER with correct level
        if type == logging.ERROR: self.logger.error(msg)
        elif type == logging.WARNING: self.logger.warning(msg)
        elif type == logging.INFO: self.logger.info(msg)
        elif type == logging.DEBUG: self.logger.debug(msg)
        else : self.logger.info(msg)


    # -------------------------------------
    # ------------ logClose() -------------
    # -------------------------------------
    # 
    # DESCRIPTION : 
    #   Cette fonction envoi le message
    #   de fin de script : OK dans les LOGS
    # 
    # PARAMÈTRES :
    #   Aucun
    #
    # RETURN :
    #   Nothing
    # 
    # USAGE :
    #   logClose()
    #
    def logClose(self):
        self.logger
        # Log End Message : OK
        self.logInfo('Fin du script : OK')


    # -------------------------------------
    # -------- logCloseAndExit() ----------
    # -------------------------------------
    # 
    # DESCRIPTION : 
    #   Cette fonction envoi le message
    #   de fin de script : OK dans les LOGS
    #   Et termine le script avec le code 0
    # 
    # PARAMÈTRES :
    #   Aucun
    #
    # RETURN :
    #   return code 0
    # 
    # USAGE :
    #   logCloseAndExit()
    #
    def logCloseAndExit(self):
        self.logClose()
        exit(0)


    # -----------------------------------------------------
    # ---------------- logCloseOnError() ------------------
    # -----------------------------------------------------
    # 
    # DESCRIPTION : 
    #   Cette fonction envoi le message
    #   de fin de script : NOK dans les LOGS
    # 
    # PARAMÈTRES :
    #   (optionel) error:str = Un message pour préciser
    #                          l'erreur avec laquelle le
    #                          script se termine 
    #
    # RETURN :
    #   Nothing
    # 
    # USAGE :
    #   logCloseOnError()
    #
    def logCloseOnError(self, error=""):
        self.logError('Fin du script : ERREUR -> ' + error)


    # --------------------------------------------------
    # ----------- logCloseAndExitOnError() -------------
    # --------------------------------------------------
    # 
    # DESCRIPTION : 
    #   Cette fonction envoi le message
    #   de fin de script : NOK dans les LOGS
    #   Et termine le script avec le code 1
    # 
    # PARAMÈTRES :
    #   (optionel) error:str = Un message pour préciser
    #                          l'erreur avec laquelle le
    #                          script se termine
    #
    # RETURN :
    #   return code 1
    # 
    # USAGE :
    #   logCloseAndExitOnError()
    #
    def logCloseAndExitOnError(self, error=""):
        self.logCloseOnError(error)
        exit(1)
