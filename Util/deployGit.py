# Version 1.0 - 2021-10-09 - Maxime Collin - Creation du script
#
# Cette lib fournit une classe DeployGit() avec des fonctions préconfigurées afin de 
#   - Cloner un dépôt
# 
# Elle s'utilise de cette façon : 
#   from Util import deployGit
#   git = deployGit.DeployGit('git@<host>:/path/to/repo.git')
#   
# Ceci va cloner le dépôt passé en paramètre dans le homedir du user, en utilisant SSH
# Au besoin une clé SSH sera générée si aucune n'est trouvée pour l'utilisateur
# 
# On utilisera un exécutable shell pour lancer les commandes SSH avec des options permettant d'éviter les prompt
# Notamment celui qui demande si on veut ajouter l'hôte du dépôt à la liste des hôtes de confiance
# Sinon ça peut être compliqué de récupérer le prompt, voir peut nécessiter l'utilisation de X11
# 
# LES FONCTIONS DISPONIBLES :
#   DeployGit(repo_url) (constructeur) initialise et clone le repo
#   gitClone() clone le repo (appelé par le constructeur)
# 

########################################################################################################################
#################################################### IMPORT MODULES ####################################################
########################################################################################################################

import os               # for path and get user uid
import re               # for check repo_url
import pwd              # for check user ssh key and get username and homedir
import socket           # for get hostname and put it in ssh key as comment
import subprocess       # for ssh-keygen
from git import Repo    # for git command
from git import Git     # for ssh
from Util import util


class DeployGit:

    ####################################################################################################################
    ################################################# DEFINE CONSTANTS #################################################
    ####################################################################################################################

    # The name of executable .sh file for use this command as ssh command
    SSH_EXE = 'deployGit_sshExe.sh'


    ####################################################################################################################
    ################################################# DEFINE FUNCTONS ##################################################
    ####################################################################################################################

    # ----------------------------------------------------------------------------
    # --------------------------------- __init__ ---------------------------------
    # ----------------------------------------------------------------------------
    #  
    # DESCRIPTION : 
    #   Le constructeur va cloner le dépôt dans le home du user en utilisant SSH
    # 
    # PARAMÈTRES :
    #   repo_url:str = URL du dépôt
    #
    # RETURN :
    #   nothing
    # 
    # EXCEPTION :
    #   ValueError if URL isn't well formatted  
    # 
    # USAGE :
    #   DeployGit("git@gitlab.com:/path/to/repo.git")
    # 
    def __init__(self, repo_url):

        # ----------------- Check if URL is well formatted ------------------
        if self.__checkGitURL(repo_url):
            self.repo_url = repo_url
        # Else -> terminated
        else:
            errorMsg = """Mauvaise URL : {0} 
            L'URL du repo doit être de la forme : git@<host>:path/to/repo.git
            """
            raise ValueError(errorMsg.format(repo_url))

        # ------------------------ Define self Attributes ------------------------
        repo_partsUrl = repo_url.split(":")
        # Extract repo's hostname from URL
        self.repo_host = repo_partsUrl[0].split('@')[1]

        # Extract repo's path from URL
        repo_urlpath = repo_partsUrl[1]
        # Extract repo's name from path
        # (basename -> just the .git filename
        # + split(.)[0] -> keep just the filename without extension = repo's name)
        self.repo_name = os.path.basename(repo_urlpath).split(".")[0]
        
        # Get the homedir of user
        self.home_dir = pwd.getpwuid(os.geteuid()).pw_dir
        # Get username
        self.user_name = pwd.getpwuid(os.geteuid()).pw_name
        # Define the local path dir for repo
        self.repo_localdir = os.path.join(self.home_dir, self.repo_name)

        self.__checkIfRepoDirIsEmpty()

        # Define the path to ssh exe file
        self.ssh_exePath = os.path.join(self.home_dir, DeployGit.SSH_EXE)

        # Get the SSH public key for user
        self.pubkey_filePath = self.__sshKeygen()
        
        # Clone the repo
        self.repo = self.gitClone()


    # -----------------------------------------------------------
    # --------------------- __checkGitURL() ---------------------
    # -----------------------------------------------------------
    #  
    # DESCRIPTION : 
    #   Cette fonction va vérifier que l'URL passée en paramètre
    #   respecte le format ssh git : git@<host>:/path/to/repo.git
    # 
    # PARAMÈTRES :
    #   repo_url:str = URL du dépôt
    #
    # RETURN :
    #   True or False
    # 
    # USAGE :
    #   checkGitURL("git@gitlab.com:/path/to/repo.git")
    # 
    def __checkGitURL(self, repo_url):
        
        urlRegex = re.compile('^git@.*:.*\.git$')
        
        if urlRegex.match(repo_url):
            return True

        return False

    # ------------------------------------------------------------------------------------------------------------------
    # -------------------------------------------- __chooseRepoDirAction() ---------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------
    #  
    # DESCRIPTION : 
    #   Cette fonction va demander à l'utilisateur quoi faire avec le répertoire local
    #   Utilisée pour le déployement des sources (git clone) qui termine en erreur si le répertoire local n'est pas vide
    # 
    # PARAMÈTRES :
    #
    # RETURN :
    #   nothing
    # 
    # USAGE :
    #   __chooseRepoDirAction()
    # 
    def __chooseRepoDirAction(self):

        print("Le script a besoin que le chemin de destination soit vide")

        # ------------------------------------------ Ask the user to choose --------------------------------------------
        # Loop while he choose
        finished = False
        while not finished:
            # Ask him to choose 
            choice = input("Souhaitez-vous renommer " + self.repo_localdir + " en " + self.repo_localdir + ".bak [O/n]")

            # If yes (or empty for default) -> finished and continue
            if choice == "O" or choice == "":
                finished = True
                # if rename action failed -> terminated
                if not util.renameFileOrDir(self.repo_localdir, self.repo_localdir + ".bak"):
                    print ("Impossible de renommer " + self.repo_localdir + " en " + self.repo_localdir + ".bak")
                    print ("Le script ne peut pas continuer, fin")
                    exit (1)

            # If no -> terminated
            elif choice == "n":
                finished = True
                print ("Arrêt du déployement des sources")
                exit (1)

            # If user's input isn't a "O" or "n" or "" -> ask him again
            else:
                print(choice)
                print ("Je n'ai pas compris ce que vous voulez dire")
                print ("Pouvez-vous recommencer ?")


    # -----------------------------------------------------------------------------------------------------------------
    # ------------------------------------------- __checkIfRepoDirIsEmpty() -------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    #  
    # DESCRIPTION : 
    #   Cette fonction va vérifier si le répertoire local pour les sources est vide ou non 
    #   git clone échoue s'il n'est pas vide
    #
    #   Si le répertoire n'est pas vide, elle va demander à l'utilisateur de choisir quoi faire 
    # 
    # PARAMÈTRES :
    #
    # RETURN :
    #   nothing
    # 
    # EXCEPTION:
    #   Exception if util.checkIfDirIsEmpty() return something strange
    # 
    # USAGE :
    #   __checkIfRepoDirIsEmpty()
    # 
    def __checkIfRepoDirIsEmpty(self):
        # Get repo state (EMPTY, NOTEXISTS, NOTEMPY OR ISNOTDIR)
        repoState = util.checkIfDirIsEmpty(self.repo_localdir)
        
        # --------------------------------------------- Check repo state ---------------------------------------------
        # If local repo is empty or doesn't exist -> OK
        if repoState == util.DirState.EMPTY or repoState == util.DirState.NOTEXISTS:
            return
        
        # If local repo ins't emtpy -> print a warning
        elif repoState == util.DirState.NOTEMPTY:
            print ("! Attention ! Le répertoire local n'est pas vide : " + self.repo_localdir)

        # If local repo ins't a dir -> print a warning
        elif repoState == util.DirState.ISNOTDIR:
            print (
                "! Attention ! Le chemin vers le répertoire local pointe sur quelque chose d'autre qu'un répertoire : "
                + self.repo_localdir
            )

        # If util.checkIfDirIsEmpty() return something else, raise an exception
        else:
            raise Exception("util.checkIfDirIsEmpty() return something strange, check it")


        # --------------------------------- Ask user to choose an action ----------------------------------
        # If we reach this point, local repo is not empty or is not a file -> ask user to choose what to do
        self.__chooseRepoDirAction()
    

    # ----------------------------------------------------------------------- 
    # ---------------------- __searchPubkeyPathFiles() ----------------------
    # -----------------------------------------------------------------------
    #  
    # DESCRIPTION : 
    #   Cette fonction va chercher sur dans un répertoire donné
    #   les clés publiques SSH et renvoyer la liste des chemins vers ces clés
    # 
    # PARAMÈTRES :
    #   ssh_dir:str = Le chemin du répertoire à analyser
    #
    # RETURN :
    #   List = La liste des chemins vers les clés publiques, peut être vide.
    # 
    # USAGE :
    #   __searchPubkeyPathFiles("/path/to/ssh/dir")
    # 
    def __searchPubkeyPathFiles(self, ssh_dir):
        # Prepare the List of public keys
        pubkeys_pathFiles = []
        # Check if ssh_dir is a directory
        if os.path.isdir(ssh_dir):
            # Loop on each dir find
            for root, dirs, files in os.walk(ssh_dir):
                # Loop on files inside dir
                for sshFile in files:
                    # If file extension is .pub -> is a public key
                    if sshFile.endswith(".pub"):
                        # Append path to this file
                        pubkeys_pathFiles.append(os.path.join(root, sshFile))

        return pubkeys_pathFiles


    # ---------------------------------------------------------------------------
    # -------------------------- __choosePubkeyFile() ---------------------------
    # ---------------------------------------------------------------------------
    # DESCRIPTION : 
    #   Cette fonction demande à l'utilisateur de choisir une clé SSH publique
    #   parmis une liste de chemin vers des clés
    # 
    # PARAMÈTRES :
    #   pubkeys_pathFiles:List = La liste de chemins vers les clés publiques
    #
    # RETURN :
    #   str = Le chemin vers la clé publique choisie
    # 
    # USAGE :
    #   __choosePubkeyFile(["/path/to/pubkey1", "/path/to/pubkey2"])
    # 
    def __choosePubkeyFile(self, pubkeys_pathFiles):
        print("Plusieurs clés publiques trouvées :")
        # ---------- Print List of available keys -----------
        # The index of List
        idx = 0
        # A String for store the list of index
        listIdx = ""
        # Loop on List of public keys
        for key in pubkeys_pathFiles:
            # Add current index to to list of index
            # (! first character will be a '/' !)
            listIdx = listIdx + "/" + str(idx)
            # Print the path to the public key with his index
            print (str(idx) + " : " + key)
            # Increment the index
            idx += 1

        # ----------------------- Ask the user to choose ------------------------
        # Loop while he choose one
        finished = False
        while not finished:
            # Ask him to choose one (he must enter a index)
            # (remove first character of list of index)
            keyidx = input("Laquelle utiliser ? [" + listIdx[1:] + "]")

            try:
                # try to access to the item in List of path to pubkeys with input
                pubkey_filePath = pubkeys_pathFiles[int(keyidx)]
                # If no Exception raised -> success, end loop
                finished = True

            # Cannot cast input as int
            except ValueError:
                print("Mauvaise valeur saise, veuillez recommencer")

            # Wrong index
            except IndexError:
                print("Mauvaise valeur saise, veuillez recommencer")
        
        # return the path to public key choosen
        return pubkey_filePath


    # -------------------------------------------------------------------------------------------
    # ---------------------------------- __sshKeygenGenerate() ----------------------------------
    # -------------------------------------------------------------------------------------------
    # 
    # DESCRIPTION : 
    #   Cette fonction génère une clé SSH à l'endroit indiqué en paramètre
    #   La clé n'aura pas de passphrase et comme commentaire :
    #   user@hostname Clé pour récupérer les sources sur gitlab
    #  
    #   Il faut éviter d'utiliser des sous-process qui demande à l'utilisateur des inputs
    #   pendant l'exécution. Sinon l'utilisation est plus contraignante, voir peut nécessiter X11
    # 
    # PARAMÈTRES :
    #   key_file:str = Le chemin vers la clé privée à créer 
    #
    # RETURN :
    #   str = Le chemin vers la clé publique créée
    # 
    # USAGE :
    #   __sshKeygenGenerate("/path/to/privatekey")
    #     
    def __sshKeygenGenerate(self, key_file):
        # Define the comment -> username@hostname + comment
        # Get the username
        # Get the hostname
        hostname = socket.gethostname()
        # Define the comment
        comment = self.user_name + "@" + hostname + " Clé pour récupérer les sources sur gitlab"

        try:
            # run ssh-keygen -f key_file -P "" -C comment :
            #   -f key_file used for define the path to key file (avoid ask user in prompt)
            #   -P "" used for define no passphrase (avoid ask user in prompt)
            #   -C comment used for define a commentary
            subprocess.run(["ssh-keygen", "-f", key_file, "-P", "", "-C", comment])
            
            # If no Exception raised -> return the path to the public key
            return key_file + ".pub"

        except subprocess.CalledProcessError:
            print ("Erreur à la génération de la clé")
            raise


    # ---------------------------------------------------------------------------------------
    # -------------------------------- __waitPublishingKey() --------------------------------
    # ---------------------------------------------------------------------------------------
    # 
    # DESCRIPTION : 
    #   Cette fonction demande à l'utilisateur de publier la clé publique
    #   sur son compte du dépôt en ligne
    #   Exemple : si le dépôt est sur gitlab, l'utilisateur devra publier
    #             la clé publique sur son compte gitlab
    # 
    #   On attend que l'utilisateur ait fini avant de continuer 
    # 
    # PARAMÈTRES :
    #   pubkey_filePath:str = Le chemin vers la clé publique 
    #
    # RETURN :
    #   nothing
    # 
    # USAGE :
    #   __waitPublishingKey("/path/to/publickey")
    #     
    def __waitPublishingKey(self, pubkey_filePath):
        # Print public key on STDOUT -> more easier to publish key on repo's host
        pubkey_file = open(pubkey_filePath, 'r')
        print (pubkey_file.read())
        
        print ("Publiez cette clé sur le compte " + self.repo_host + " de " + self.user_name)

        # Wait user as finished
        finished = False
        while not finished:
            # Ask if it's done (by default, No)
            done = input("C'est fait ? [o/N]")

            # If yes -> finished and continue
            if done == "o":
                finished = True

            # If No (or empty for default) -> terminated
            elif done == "N" or done == "":
                finished = True
                print ("Le script ne peut pas continuer tant que la clé n'est pas publiée")
                print ("Publiez la clé, puis relancez le script")
                exit (1)

            # If user's input isn't a "o" or "N" or "" -> ask him again
            else:
                print(done)
                print ("Je n'ai pas compris ce que vous voulez dire")
                print ("Pouvez-vous recommencer ?")


    # ------------------------------------------------------------------
    # ------------------------- __sshKeygen() --------------------------
    # ------------------------------------------------------------------
    # 
    # DESCRIPTION : 
    #   Cette fonction va chercher une clé publique sur le système 
    #   si elle n'en trouve pas, elle l'a générera 
    # 
    # PARAMÈTRES :
    #   Aucun
    #
    # RETURN :
    #   str = Chemin vers la clé publique
    # 
    # USAGE :
    #   __sshKeygen()
    #     
    def __sshKeygen(self):
        print ("Recherche d'une clé SSH publique")

        pubkey_filePath = ""

        # Define the ssh directory : is in the home_dir
        ssh_dir = os.path.join(self.home_dir, ".ssh")

        # Looking for public keys in ssh directory
        pubkeys_pathFiles = self.__searchPubkeyPathFiles(ssh_dir)

        # If there is many -> ask user to choose one
        if len(pubkeys_pathFiles) > 1:
            pubkey_filePath = self.__choosePubkeyFile(pubkeys_pathFiles)
    
        # If there is only one -> we find it
        elif len(pubkeys_pathFiles) == 1:
            pubkey_filePath = pubkeys_pathFiles[0]
            print ("clé publique trouvée : " + pubkey_filePath)

        # If there isn't -> generate it
        else:
            print ("Pas de clé publique trouvée -> ssh-keygen")
            # Define the path to private key file 
            key_file = os.path.join(ssh_dir, "id_rsa")
            try:
                # Generate the key and get back the path to public key
                pubkey_filePath = self.__sshKeygenGenerate(key_file)
            
            # If there is a pb -> raise the Exception
            except subprocess.CalledProcessError:
                raise 

        # Wait for user has finished to publish to key on repo
        self.__waitPublishingKey(pubkey_filePath)

        return pubkey_filePath
        

    # ----------------------------------------------------------------------------------------------------------------
    # ------------------------------------------------ __genSSHexe() -------------------------------------------------
    # ----------------------------------------------------------------------------------------------------------------
    # 
    # DESCRIPTION : 
    #   Cette fonction génère un fichier .sh qui lance la commande SSH avec l'option -o StrictHostKeyChecking=no
    #   -> permet de ne pas demander à l'utilisateur s'il veut ajouter cet hôte à ces hôtes de confiance
    #   Il faut donner les droits en exécution sur le fichier
    # 
    #   Il faut éviter d'utiliser des sous-process qui demande à l'utilisateur des inputs
    #   pendant l'exécution. Sinon l'utilisation est plus contraignante, voir peut nécessiter X11
    # 
    # PARAMÈTRES :
    #   privatekey_filePath:str = Le chemin vers la clé privée à utiliser 
    #
    # RETURN :
    #   nothing
    # 
    # USAGE :
    #   __genSSHexe("/path/to/privatekey")
    #     
    def __genSSHexe(self, privatekey_filePath):
        print("Génération d'un exécutable .sh pour l'utiliser comme commande SSH lors de la connexion au serveur Git")

        # Open the file with write permission
        ssh_exe = open(self.ssh_exePath, 'w')
        # Write the file
        # Define interpreter
        ssh_exe.write("#!/bin/sh\n")
        # Define a var for path to private key
        ssh_exe.write("ID_RSA=" + privatekey_filePath + "\n")
        # Define the new command for use SSH with options :
        #   -o StrictHostKeyChecking=no for avoid prompt during execution
        #   -i $ID_RSA for define private key to use
        #   $@ for put all other options give in args
        ssh_exe.write('exec /usr/bin/ssh -o StrictHostKeyChecking=no -i $ID_RSA "$@"')

        # Set execution permission on it
        os.chmod(self.ssh_exePath, 0o755)


    # -----------------------------------------------------------------------------------------------
    # ----------------------------------------- gitClone() ------------------------------------------
    # -----------------------------------------------------------------------------------------------
    # 
    # DESCRIPTION : 
    #   Cette fonction va cloner le dépôt
    # 
    # PARAMÈTRES :
    #   Aucun
    #
    # RETURN :
    #   Repo = Un objet de la lib Repo qui pointe vers le dépôt
    # 
    # USAGE :
    #   gitClone()
    #     
    def gitClone(self):
        # Generate the .sh file for change the SSH command
        self.__genSSHexe(self.pubkey_filePath[:len(self.pubkey_filePath)-4])

        print ("Récupération des sources")
        # Give the repo's url, the local repo's dir and the ENV var GIT_SSH for use this as ssh cmd
        return Repo.clone_from(self.repo_url, self.repo_localdir, env=dict(GIT_SSH=self.ssh_exePath))

