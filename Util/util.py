# Version 1.0 - 2020-04-19 - Cyril CARGOU - Lib de fonctions génériques et/ou preconfiguré
# Version 2.0 - 2021-10-09 - Maxime Collin - Séparation des fonctions de logs du reste
# 
# Cette lib founit une liste de fonctions génériques
# 
# LISTE DES FONCTIONS DISPONIBLES :
#   - getScriptFullPath()   -> Renvoi le chemin absolu du 1er script appelé (ie. le script mère)
#   - getScriptDirPath()    -> Renvoi le chemin absolu du répertoire dans lequel le 1er script se trouve
#   - getScriptName()       -> Renvoi le nom du 1er script
# 
#   - getScriptParameter(paramName, defaultValue)  -> Renvoi la valeur d'un argument passé au 1er script
# 
#   - checkIfCurrentUserIsSUDOER() -> Renvoi True si le script est exécuté avec les droits sudo
# 
#   - createGroup(group_name)                -> Créé un groupe système 
#   - createDir(dir_path)                    -> Créé le répertoire passé en paramètre
#   - renameFileOrDir(originalName, newName) -> Renomme un fichier ou un répertoire
#   - checkIfDirIsEmpty(dir_path)            -> Vérifie si un répertoire est vide
# 
#   - searchAndReplaceTxtOrInsert(txt, repl, string, pos)   -> Cherche le texte dans le string et le 
#                                                              remplace par repl. S'il ne le trouve pas
#                                                              il l'insert à la position pos
#

########################################################################################################
############################################ IMPORT MODULES ############################################
########################################################################################################

import os,sys,shutil          # for use some system command
import grp                    # for work on group
import subprocess             # for call unix command groupadd
from enum import Enum         # for create enumeration
import re                     # for use regex


########################################################################################################
########################################### DEFINE FUNCTIONS ###########################################
########################################################################################################

# ------------------------------------------------------------------------
# ------------------------- getScriptFullPath() --------------------------
# ------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction va renvoyer le chemin absolu vers le 1er script appelé
#   Exemple 1 : Si on créé un fichier foo.py qui appelle cette fonction
#     foo.py
#       from Util import util
#       util.getScriptFullPath()
#  
#     max@host:$ python foo.py 
#     /path/to/foo.py
# 
#   Exemple 2 : Si on créé un fichier bar.py qui appelle le fichier foo.py
#     bar.py
#       from foo import *
# 
#     max@host:$ python bar.py
#     /path/to/bar.py
# 
# PARAMÈTRES :
#   Aucun
# 
# RETURN :
#   Un string qui représente le chemin absolu 
# 
# USAGE :
#   getScriptFullPath()
# 
def getScriptFullPath():
    return os.path.abspath(sys.argv[0])


# -------------------------------------------------------------------------------------
# -------------------------------- getScriptDirPath() ---------------------------------
# -------------------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction va renvoyer le chemin absolu vers le répertoire du 1er script appelé
#   Voir getScriptFullPath() pour l'exemple 
# 
# PARAMÈTRES :
#   Aucun
# 
# RETURN :
#   Un string qui représente le chemin absolu 
# 
# USAGE :
#   getScriptDirPath()
# 
def getScriptDirPath():
    return os.path.abspath(os.path.dirname(getScriptFullPath()))


# --------------------------------------------------------
# ------------------- getScriptName() --------------------
# -------------------------------------------------------- 
# 
# DESCRIPTION :
#   Cette fonction va renvoyer le nom du 1er script appelé
#   Voir getScriptFullPath() pour l'exemple 
# 
# PARAMÈTRES :
#   Aucun
# 
# RETURN :
#   Un string qui représente le nom du script
# 
# USAGE :
#   getScriptName()
# 
def getScriptName():
    return os.path.basename(getScriptFullPath())


# -------------------------------------------------------------------------------
# ---------------------------- getScriptParameter() -----------------------------
# -------------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction recherche un paramètre dans les arguments du 1er script appelé
#   Il peut être de forme
#       --paramName -> renvoi True s'il est trouvé 
#       ou --paramName=value -> renvoi value s'il est trouvé
#   S'il n'est pas trouvé, renvoi defaultValue 
# 
# PARAMÈTRES :
#   paramName:str    = le nom du paramètre sans les '--',
#                      avec le '=' si on attend une valeur
#   defaultValue:obj = La valeur par défaut renvoyée
#                      si paramName n'a pas été trouvé
#                      (peut être de n'importe quel type) 
# 
# RETURN :
#   True si le paramètre est de la forme --paramName
#   Str si le paramètre est de la forme --paramName=
#   defaultValue:Obj si le paramètre n'a pas été trouvé 
# 
# USAGE :
#   getScriptParameter('check', False) -> Renvoi True si --check est présent,
#                                         False sinon
#   getScriptParameter('day=', 5) -> Renvoi ce qui suit --day= si trouvé, 5 sinon
# 
def getScriptParameter(paramName, defaultValue):

    #parcour la liste des arguments
    for a in sys.argv[1:]:
        #si --day= ou --whatIf est dans le tableau
        if a.startswith("--"+paramName) == True:
            #argument trouvé, test si argument swtich
            if paramName.find("=") > -1:
                #ce n'set pas un switch
                return  a.split("=")[1]
            else:
                # c'est un swtich
                return True
    #si non trouvé, retour la valeur par défaut               
    return defaultValue


# --------------------------------------------------------------------------
# ------------------------ checkIfCurrentIsSUDOER() ------------------------
# --------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction va vérifier si le script est exécuté avec les droits sudo
# 
# PARAMÈTRES :
#   Aucun
# 
# RETURN :
#   True si oui
#   False sinon 
# 
# USAGE :
#   checkIfCurrentIsSUDOER()
# 
def checkIfCurrentIsSUDOER():
    # Ckeck if script launch with root permission, if not -> terminated
    if os.geteuid() != 0:
        return False
    return True


# -------------------------------------------------------------------------------
# -------------------------------- createGroup() --------------------------------
# ------------------------------------------------------------------------------- 
# 
# DESCRIPTION :
#   Cette fonction va créer un groupe système
#   Il faut que le script qui la lance utilise les droits sudo 
# 
# PARAMÈTRES :
#   group_name:str = Le nom du groupe à créer
# 
# RETURN :
#   return code 1 si la création du groupe échoue
# 
# EXCEPTION:
#   subprocess.CalledProcessError : Lève une exception si la création
#                                   du groupe échoue
# 
# USAGE :
#   createGroup('myGroup')
# 
def createGroup(group_name):

    # Ckeck if script launch with root permission, if not -> terminated
    if not checkIfCurrentIsSUDOER():
        print ("Erreur : Vous n'avez pas les droits pour exécuter ce script")
        print ("Fin du script")
        exit(1)

    # grp.getgrnam() can raise a KeyError if group doesn't exist
    try:
        # Seach if group exist
        grp.getgrnam(group_name)
        print ("Le groupe " + group_name + " existe déjà")


    except KeyError:
        # KeyError raise -> group doesn't exist
        # subprocess.run can raise a CalledProcessedError
        # if command return a status code != 0
        try:
            # Create group
            print("Création du groupe " + group_name)
            subprocess.run(["groupadd", group_name])
        
        except subprocess.CalledProcessError:
            # groupadd in error -> terminated
            print ("Erreur : la création du groupe " + group_name + " a échouée")
            raise


# ---------------------------------------------------------
# ---------------------- createDir() ----------------------
# ---------------------------------------------------------
#  
# DESCRIPTION :
#   Cette fonction va créer un répertoire
# 
# PARAMÈTRES :
#   group_name:str = Le chemin du répertoire
#                    peut être absolu ou relatif 
# 
# RETURN :
#   return code 1 si la création du groupe échoue
# 
# EXCEPTION :
#   PermissionError : Lève une exception si l'utilisateur
#   n'a pas les droits de créer le répertoire
#  
# USAGE :
#   createDir('path/to/dir')
# 
def createDir(dir_path):
    # os.mkdir() raise a FileExistsError if dir exist
    try:
        # Create the dir
        print ("Création du répertoire " + dir_path)
        os.mkdir(dir_path)

    except FileExistsError:
        print("Le répertoire " + dir_path + " existe déjà")


# --------------------------------------------------------------------------
# --------------------------- renameFileOrDir() ----------------------------
# --------------------------------------------------------------------------
#  
# DESCRIPTION :
#   Cette fonction va renommer un fichier ou un répertoire
# 
# PARAMÈTRES :
#   originalName:str = le chemin vers le fichier ou le répertoire à renommer
#   newName:str = le nouveau nom du fichier ou du répertoire
# 
# RETURN :
#   return True or False
#  
# USAGE :
#   renameFileOrDir('fileOrDirToRename', 'newName')
# 
def renameFileOrDir(originalName, newName):
    if os.path.exists(newName):
        print ("Erreur : le chemin de destination existe déjà")
        return False
    
    if not os.path.exists(originalName):
        print ("Erreur : le chemin source n'existe pas")
        return False

    # shutil.move() return the new name if everything is ok
    if shutil.move(originalName, newName) != newName:
        print ("Erreur lors du renommage")
        return False

    return True


# -------------------------------------------
# ----------- checkIfDirIsEmpty() -----------
# -------------------------------------------
#  
# DESCRIPTION :
#   Cette fonction va vérifier si le
#   répertoire donné en paramètre est vide
#   Elle utilise un énumérateur pour 
#   les codes retours 
# 
# PARAMÈTRES :
#   dir_path:str = le chemin vers le
#                  répertoire à vérifier
# 
# RETURN :
#   return un entier défini par DirState
#  
# USAGE :
#   checkIfDirIsEmpty('/dir/path')
# 


# L'énumérateur utilisé par la fonction
# Plusieurs états possibles :
#   - Répertoire vide
#   - Répertoire non vide
#   - Le chemin n'est pas un répertoire
#   - Le chemin n'existe pas 
class DirState(Enum):
    EMPTY = 0
    NOTEMPTY = 1
    ISNOTDIR = 2
    NOTEXISTS = 3

# La fonction elle-même
def checkIfDirIsEmpty(dir_path):
    # Check if dir exists
    if os.path.exists(dir_path):
        # If yes -> check if dir is a dir
        if os.path.isdir(dir_path):
            # If yes -> check if dir is empty
            if not os.listdir(dir_path):
                return DirState.EMPTY
            
            # No the dir is not empty
            else:
                return DirState.NOTEMPTY
        
        # No the dir isn't a dir 
        else:
            return DirState.ISNOTDIR

    # No the dir isn't exists
    else:
        return DirState.NOTEXISTS


# ----------------------------------------------------------------------------------------
# ---------------------------- searchAndReplaceTxtOrInsert() -----------------------------
# ----------------------------------------------------------------------------------------
#  
# DESCRIPTION :
#   Cette fonction va chercher un texte à remplacer dans un string
#   Si elle ne trouve pas, elle insert le nouveau texte à la position indiquée
#   Sur une nouvelle ligne
# 
# PARAMÈTRES :
#   txt:str    = le texte à remplacer, peut être une expression régulière
#   repl:str   = le nouveau texte
#   string:str = le string dans lequel effectué l'opération
#   pos:int    = la position à laquelle insérer le nouveau texte si txt n'a pas été trouvé
# 
# RETURN :
#   retourne le string modifié
# 
#  
# USAGE :
#   searchAndReplaceTxtOrInsert('toto', 'titi', 'Welcome toto', 8)
#     -> 'Welcome titi'
# 
#   searchAndReplaceTxtOrInsert('toto', 'titi', 'Welcome ', len('Welcome '))
#     -> 'Welcome 
#         titi'
# 
def searchAndReplaceTxtOrInsert(txt, repl, string, pos):

    # compile in case is a regular exception
    pattern = re.compile(txt)

    # if pattern doesn't find
    if pattern.search(string) is None:
        # -> insert repl at pos
        string = string[:pos] + '\n' + repl + '\n' + string[pos:]
    else:
        # pattern has been find -> replace it by repl
        string = re.sub(pattern, repl, string)

    return string