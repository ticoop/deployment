# Version 1.0 - 2021-10-09 - Maxime Collin - Creation du script
#
# Ce script va créer un groupe pour écrire des fichiers de logs dans un répertoire dédié
# Il va aussi créé le répertoire en question
# 
# USAGE :
#   Assurez-vous que la variable d'environnement PYTHONPATH pointe bien sur le répertoire du script (Util)
#   Et que (Defaults        env_keep += "PYTHONPATH") soit défini dans /etc/sudoers ou dans un fichier dans /ets/sudoers.d/
#   Ensuite lancez la commande : 
#   sudo python createLogGroup.py
# 


###########################################################################################################################
####################################################### IMPORT MODULES ####################################################
###########################################################################################################################

import grp              # for search if group exist and get back is gid
import os               # for mkdir, chow and chmod function
import sys              # for uncaught exceptions
from Util import util   # A custom lib for check if user is sudoer, create group and dir
from Util import log    # A custom lib for get log group name and folder


###########################################################################################################################
##################################################### DEFINE FUNCTIONS ####################################################
###########################################################################################################################

# -------------------------------------------------------------------------------------------------------------
# ------------------------------------------ setPermissionsLogDir() -------------------------------------------
# -------------------------------------------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction va changer le groupe du répertoire log.Log.ROOTFOLDER pour le groupe log.Log.LOGGROUP
#   Et lui attribuer les droits en écriture
# 
# PARAMÈTRES :
#   Aucun
# 
# RETURN :
#   nothing
# 
# USAGE :
#   setPermissionsLogDir()
# 
def setPermissionsLogDir():
    print ("Attribution des droits au groupe " + log.Log.LOGGROUP + " sur le répertoire " + log.Log.ROOTFOLDER)
    # Change group of dir to created group. Args are : 
    #   the directory (log.Log.ROOTFOLDER)
    #   the uid of new owner (-1 for unchanged owner)
    #   the gid of new group (grp.getgrnam(log.Log.LOGGROUP) for find the group and .gr_gid for getback the gid)
    # Note : grp.getgrnam() can't raise a KeyError because the group exist
    os.chown(log.Log.ROOTFOLDER, -1, grp.getgrnam(log.Log.LOGGROUP).gr_gid)

    # And give to the group the write permission. Args are :
    #   the directory (log.Log.ROOTFOLDER)
    #   the new mode in octal (0o775 for rwx rwx r-x)
    os.chmod(log.Log.ROOTFOLDER, 0o775)


###########################################################################################################################
########################################################### MAIN ##########################################################
###########################################################################################################################

try:
    # Ckeck if script launch with root permission, if not -> terminated
    if not util.checkIfCurrentIsSUDOER():
        print ("Erreur : Vous n'avez pas les droits pour exécuter ce script")
        print ("Fin du script")
        exit(1)
    # -> no PermissionError can be raise on the following because we have root permission

    # Create Group
    try:
        util.createGroup(log.Log.LOGGROUP)

    except subprocess.CalledProcessError:
        print("Fin du script")
        exit(1)

    # Create Directory
    util.createDir(log.Log.ROOTFOLDER)

    # Set Permissions on dir
    setPermissionsLogDir()

except:
    print ("Erreur : ")
    print (sys.exc_info()[0])
    exit(1)