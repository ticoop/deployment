#!/bin/bash

# 
# Version 1.0 - 2021-10-10 - Maxime Collin - Creation du script
#
# Ce script est une librairie d'utilitaires shell
# Il s'appelle avec source util.sh pour charger les fonctions
# 
# LISTE DES FONCTIONS DISPONIBLES :
#   - sendErrorMsg "Mon message d'erreur" -> renvoi le message d'erreur sur STDERR avec un pré-formattage
# 
#   - ckeckIfCurrentUserIsSUDOER -> renvoi $TRUE si l'utilisateur courant est sudoer, $FALSE sinon
# 
#   - checkIfCurrentUserHasWritePermissionOnPATH /path/to/check -> renvoi $TRUE si l'utilisateur courant
#     a les droits en écriture sur le chemin passé en paramètre, $FALSE sinon
# 
#   - installRPMpackage paquet1 paquet2 ... -> installe les paquets passés en paramètres
# 
#   - createPython3Symlink /path/to/symlink -> crée le lien symbolique en paramètre sur le binaire python3
# 
# LISTE DES CONSTANTES DISPONIBLES :
#   - TRUE = 0
#   - FALSE = 1
# 
#   - MSG_END_SCRIPT_ON_ERROR à envoyer à sendErrorMsg avant un exit 1
# 
#   - RED pour pré-formatté un message avec la couleur rouge
#   - NC pour enlever la couleur d'un message pré-formatté
# 

###################################################################################################################################################
################################################################# DEFINE CONSTANT #################################################################
###################################################################################################################################################
declare -r TRUE=0
declare -r FALSE=1

declare -r RED='\033[0;31m'
declare -r NC='\033[0m' # No Color

declare -r MSG_END_SCRIPT_ON_ERROR="${RED}Fin du script"

declare -r UTIL_PIPPACKAGES_NEEDED="Util/requirements.txt"
declare -r UTIL_RPMPACKAGES_NEEDED="python3 pip git ssh ssh-askpass"


###################################################################################################################################################
################################################################# DEFINE FUNCTION #################################################################
###################################################################################################################################################

# ------------------------------------------------------------
# ---------------------- sendErrorMsg() ----------------------
# ------------------------------------------------------------
# 
# DESCRIPTION : 
#   Cette fonction envoi un message d'erreur sur STDERR
# 
# PARAMÈTRES :
#   $1 = errorMsg : string -> Un message pré-formatté (printf)
#
# RETURN :
#   "Erreur : ${1}"
#   "Fin du script"
#   sur STDERR
# 
# USAGE :
#   sendErrorMsg "Mon message d'erreur\nPréformatté"
# 
sendErrorMsg()
{
    >&2 printf "${RED}Erreur :${NC} ${1}\n"
}


# -------------------------------------------------------------------------------------------
# ------------------------------ ckeckIfCurrentUserIsSUDOER() -------------------------------
# -------------------------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction check si l'utilisateur courant est dans le groupe sudo
#   Si ce n'est pas le cas, il renvoi FALSE (1)
#   Utilisé par des scripts pour vérifier que l'utilisateur a les droits de les exécuter
# 
# PARAMÈTRES :
#   Aucun
# 
# RETURN :
#   TRUE (0) si le user est sudoer
#   FALSE (1) sinon
# 
# USAGE :
#   checkIfCurrentUserIsSUDOER && echo "Vous êtes sudoer" || { 
#                                                               echo "Vous n'etes pas sudoer"
#                                                               exit 1
#                                                            }
checkIfCurrentUserIsSUDOER()
{
    # id -nG return list of current user's group
    # grep -q for quiet (no prompt) and -w for all word
    if ( id -nG | grep -qw "sudo")
    then
        return $TRUE
    else
        return $FALSE
    fi
}


# ------------------------------------------------------------------------------------------------------------------
# ---------------------------------- checkIfCurrentUserHasWritePermissionOnPATH() ----------------------------------
# ------------------------------------------------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction check si l'utilisateur courant a les droits en écriture sur le chemin passé en paramètre
#   Si ce n'est pas le cas, il renvoi FALSE (1)
# 
# PARAMÈTRES :
#   $1 = /path/to/check
# 
# RETURN :
#   TRUE (0) si le user a les droits en écriture
#   FALSE (1) sinon
# 
# USAGE :
#   checkIfCurrentUserHasWritePermissionOnPATH && echo "Vous pouvez écrire" || { 
#                                                                                   echo "Vous ne pouvez pas écrire"
#                                                                                   exit 1
#                                                                               }
checkIfCurrentUserHasWritePermissionOnPATH()
{
    # If there is not exactly 1 argument -> terminated
    if [ $# != 1 ]
    then
        sendErrorMsg "Mauvais nombre d'arguments passés en paramètre, la fonction ne peut pas s'exécuter"
        sendErrorMsg "Usage : checkIfCurrentUserHasWritePermissionOnPATH /path/to/check"
        sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
        exit 1
    fi

    DIR_TO_CHECK=$1

    # Check if current user has permission to write
    if [[ -w $DIR_TO_CHECK ]] 
    then 
        return $TRUE 
    else
        return $FALSE
    fi
}


# ----------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------- installRPMpackage() ----------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction installe les paquets RPM passés en paramètres via apt install 
#   Elle appel sudo
# 
# PARAMÈTRES :
#   $1 = paquet1
#   $2 = paquet 2
#   ...
#   Si aucun paramètre n'est fourni, fin en erreur
# 
# RETURN :
#   le code retour de apt install s'il y a une erreur
#   0 sinon
# 
# USAGE :
#   installRPMpackage package1 package 2 ...
# 
installRPMpackage()
{
    # --------------------------------------------------- Check Parameters ---------------------------------------------------
    # If there is no argument -> terminated
    if [ $# -eq 0 ]
    then
        sendErrorMsg "Aucun arguments passés en paramètre, la fonction ne peut pas s'exécuter"
        sendErrorMsg "Usage : installRPMpackage paquet1 paquet2 paquet3 ..."
        sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
        exit 1
    fi


    # -------------------------------------------------- Check Permissions ---------------------------------------------------
    # Check if user has right, if not (||) -> terminated
    checkIfCurrentUserIsSUDOER || { 
                                    sendErrorMsg "Vous n'avez pas les droits pour exécuter cette fonction (installRPMPackage)"
                                    sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
                                    exit 1
                                  }


    # --------------------------------------------------------- MAIN ---------------------------------------------------------
    # -------------- Prepare Variables ---------------
    # Get the list of PATH for binairies
    # Setting IFS (input field separator) value as ":"
    IFS=':'
    # Split PATH into array
    read -ra PATH_ARRAY <<< "$PATH"

    # ---------------------- Start Loop On Parameters ----------------------
    # Loop on arguments
    for PACKAGE in "$@"
    do

        # ----- Looking If Package Already Installed ------
        # By default, consider that package isn't installed
        INSTALLED="false"

        # Loop on PATH
        for PATH_CURRENT in "${PATH_ARRAY[@]}"
        do
            # Search f a binairie of package exist
            if [ -f "${PATH_CURRENT}/${PACKAGE}" ]
            then
                # If it is, the package is installed
                INSTALLED="true"
                echo "${PACKAGE} est déjà installé"
                # Stop loop
                break
            fi
        done

        # ---------------------- If Not, Installed It ----------------------
        if [ $INSTALLED = "false" ]
        then
            echo "Installation du paquet ${PACKAGE}"
            sudo apt install ${PACKAGE}
            RETURN_CODE=$?

            # If something going wrong -> terminated
            if [ $RETURN_CODE != 0 ]
            then
                sendErrorMsg "le paquet ${PACKAGE} n'a pas pu être installé"
                sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
                exit $RETURN_CODE
            fi
        fi
    done
}


# --------------------------------------------------------------------------------------------
# ----------------------------------- installPipPackage() ------------------------------------
# --------------------------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction installe les paquets python passés en paramètres via pip install package
#   Ou si c'est un fichier de dépendances via pip install -r requirementsFile
#   
#   L'option --sudoer peut être ajouté comme 1er argument si on veut utiliser sudo
# 
# PARAMÈTRES :
#   $1 = paquet1 ou un fichier de dépendances
#   $2 = paquet2
#   ...
#   Si aucun paramètre n'est fourni, fin en erreur
# 
# RETURN :
#   le code retour de pip install s'il y a une erreur
#   0 sinon
# 
# USAGE :
#   installPipPackage [--sudoer] package1 package 2 ...
#   ou installPipPackage [--sudoer] requirementsFile
# 
installPipPackage()
{

    USE_SUDO=$FALSE

    # ----------------------------------- Check Parameters -----------------------------------
    # If there is no argument -> terminated
    if [ $# -eq 0 ]
    then
        sendErrorMsg "Aucun arguments passés en paramètre, la fonction ne peut pas s'exécuter"
        sendErrorMsg "Usage : installRPMpackage paquet1 paquet2 paquet3 ..."
        sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
        exit 1
    fi


    # -------------------------------------------------- Check Permissions ---------------------------------------------------
    # Check if user has right, if not (||) -> terminated
    if [ $1 = "--sudoer" ]
    then
        checkIfCurrentUserIsSUDOER || { 
                                    sendErrorMsg "Vous n'avez pas les droits pour exécuter cette fonction (installPipPackage)"
                                    sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
                                    exit 1
                                  }

        USE_SUDO=$TRUE
        shift 1
    fi

    # ----------------------------------------- MAIN -----------------------------------------
    # -------------- Prepare Variables ---------------

    PACKAGE_OR_FILE=$1

    # Check if it's a list of packages or a requirements file
    if [ -f $PACKAGE_OR_FILE ]
    then
        # ---------------------- A requirements file -----------------------
        echo "Installation du fichier de dépendances ${PACKAGE_OR_FILE}"
        if [ $USE_SUDO = $TRUE ]
        then
            sudo pip install -r $PACKAGE_OR_FILE
        else
            pip install -r $PACKAGE_OR_FILE --no-cache-dir --user
        fi
        RETURN_CODE=$?

        # If something going wrong -> terminated
        if [ $RETURN_CODE != 0 ]
        then
            sendErrorMsg "Au moins une dépendance n'a pas pu être installée"
            sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
            exit $RETURN_CODE
        fi
    else

        # ------------------------- A list of packages -------------------------
        # Loop on arguments
        for PACKAGE in "$@"
        do

            # ----- Looking If Package Already Installed ------
            pip show $PACKAGE &> /dev/null
            INSTALLED=$?

            # ---------------------- If Not, Installed It ----------------------
            if [ $INSTALLED = 1 ]
            then
                echo "Installation du paquet ${PACKAGE}"
                if [ $USE_SUDO = $TRUE ]
                then
                    sudo pip install pip install ${PACKAGE}
                else
                    pip install pip install ${PACKAGE}
                fi
                RETURN_CODE=$?

                # If something going wrong -> terminated
                if [ $RETURN_CODE != 0 ]
                then
                    sendErrorMsg "le paquet ${PACKAGE} n'a pas pu être installé"
                    sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
                    exit $RETURN_CODE
                fi
            fi
        done
    fi
}


# 
# ------------------------------------------------------------ createPython3Symlink() -------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction va créer un lien symbolique qui pointera sur le binaire python3
#   Elle utilisera sudo si l'utilisateur n'a pas les droits d'écrire dans le répertoire de destination,
#   à condition que l'utilisateur soit sudoer
# 
# PARAMÈTRES :
#   $1 = "/path/to/symlink" : str -> le chemin où sera crée le lien symbolique avec son nombre
# 
# RETURN :
#   le code retour de ln s'il y a une erreur
#   0 sinon
# 
# USAGE :
#   createPython3Symlink /path/to/symlink
# 
createPython3Symlink()
{
    # ------------------------------------------------------------- Check Parameters --------------------------------------------------------------
    # If there is not exactly 1 argument -> terminated
    if [ $# != 1 ]
    then
        sendErrorMsg "Mauvais nombre d'arguments passés en paramètre, la fonction ne peut pas s'exécuter"
        sendErrorMsg "Usage : createPython3Symlink /path/to/symlink"
        sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
        exit 1
    fi


    # ------------------------------------------------------------- Check Permissions -------------------------------------------------------------
    PATH_TO_SYMLINK=$1
    # Check if user has write permission on directory where the symlink will be created (|| -> false)
    checkIfCurrentUserHasWritePermissionOnPATH $(dirname "${PATH_TO_SYMLINK}") || WRITE_PERMISSION=$FALSE

    # If not, check if user is a sudoer for use sudo to create the symlink
    if [ $WRITE_PERMISSION = $FALSE ]
    then
        # Check if user has right, if not (||) -> terminated
        checkIfCurrentUserIsSUDOER || { 
                                        sendErrorMsg "Vous n'avez pas les droits pour écrire le lien symbolique à cet endroit (${PATH_TO_SYMLINK})"
                                        sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
                                        exit 1
                                    }
    fi


    # ------------------------------------------------------------------- MAIN --------------------------------------------------------------------
    # -------------- Prepare Variables ---------------
    PATH_TO_PYTHON3="null"

    # Get the list of PATH for binairies
    # Setting IFS (input field separator) value as ":"
    IFS=':'
    # Split PATH into array
    read -ra PATH_ARRAY <<< "$PATH"

    # ------------- Looking For Python3 Binairie ---------------
    # Loop on PATH
    for PATH_CURRENT in "${PATH_ARRAY[@]}"
    do
        # Search python3 binairie
        if [ -f "${PATH_CURRENT}/python3" ]
        then
            PATH_TO_PYTHON3="${PATH_CURRENT}/python3"
            break
        fi
    done

    # If python3 hasn't been found -> terminated
    if [ $PATH_TO_PYTHON3 = "null" ]
    then
        sendErrorMsg "python3 n'a pas été trouvé, fin du script"
        sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
        exit 1
    fi

    # ------------------------------- Check $PATH_TO_SYMLINK --------------------------------
    # Check if symlink python already exist
    if [ -L $PATH_TO_SYMLINK ]
    then
        # if symlink exist, test if target is python3
        if readlink $PATH_TO_SYMLINK | grep -q "python3"
        then
            echo "Lien symbolique python déjà créé"
        
        # else, it's not the good target -> terminated
        else
            sendErrorMsg "Lien symbolique existe mais ne pointe pas vers python3"
            sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
            exit 1
        fi
    # symlink doesn't exist but a regular file ?
    elif [ -f $PATH_TO_SYMLINK ]
    then
        # END
        sendErrorMsg "Un fichier ${PATH_TO_SYMLINK} existe mais n'est pas un lien symbolique"
        sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
        exit 1
    
    # ------------------------------- Create Symlink --------------------------------
    else
        # create symlink for use python3 as default
        echo "Création d'un lien symbolique de python3 vers python"
        if [ $WRITE_PERMISSION = "TRUE" ]
        then
            ln -s $PATH_TO_PYTHON3 $PATH_TO_SYMLINK
        else
            sudo ln -s $PATH_TO_PYTHON3 $PATH_TO_SYMLINK
        fi

        # get return code of ln
        RETURN_CODE=$?

        # If something going wrong -> terminated
        if [ $RETURN_CODE != 0 ]
        then
            sendErrorMsg "le lien symbolique ${PATH_TO_SYMLINK} n'a pas pu être créé"
            sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
            exit $RETURN_CODE
        fi
    fi
}


# --------------------------------------------------------------------------------------------------------
# ---------------------------------- allowENVvarPYTHONPATHforSudoCmd() -----------------------------------
# --------------------------------------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction va placer le fichier keepENVvarPYTHONPATH dans le répertoire /etc/sudoers.d
#   En s'assurant que le fichier appartient à root:root et n'a des droits qu'en lecture 440
# 
# PARAMÈTRES :
#   Aucun
# 
# RETURN :
#   le code retour de ln s'il y a une erreur
#   0 sinon
# 
# USAGE :
#   allowENVvarPYTHONPATHforSudoCmd
# 
allowENVvarPYTHONPATHforSudoCmd()
{
    # ----------------------------------------- Check Parameters -----------------------------------------
    # If there some arguments -> terminated
    if [ $# != 0 ]
    then
        sendErrorMsg "Mauvais nombre d'arguments passés en paramètre, la fonction ne peut pas s'exécuter"
        sendErrorMsg "Usage : allowENVvarPYTHONPATHforSudoCmd"
        sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
        exit 1
    fi


    # ----------------------------------------- Check Permissions -----------------------------------------
    SUDOER_FILE="keepENVvarPYTHONPATH"
    PATH_TO_SUDOER_FILE="${PWD}/${SUDOER_FILE}"
    SUDOER_FOLDER="/etc/sudoers.d"

    # If not, check if user is a sudoer for use sudo to create the symlink
    if [ -f $PATH_TO_SUDOER_FILE ]
    then
        # Check if user has right, if not (||) -> terminated
        checkIfCurrentUserIsSUDOER || { 
                                        sendErrorMsg "Vous n'avez pas les droits pour sudo"
                                        sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
                                        exit 1
                                    }
    else
        sendErrorMsg "Le fichier ${PATH_TO_SUDOER_FILE} n'existe pas"
        sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
        exit 1
    fi


    # ----------------------------------------------- MAIN ------------------------------------------------
    # ------------------------------------- Copy the file in /etc/sudoers.d -------------------------------------
    sudo cp $PATH_TO_SUDOER_FILE $SUDOER_FOLDER
    RETURN_CODE=$?

    # If something going wrong -> terminated
    if [ $RETURN_CODE != 0 ]
    then
        sendErrorMsg "Impossible de copier le fichier ${PATH_TO_SUDOER_FILE} dans le répertoire ${SUDOER_FOLDER}"
        sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
        exit $RETURN_CODE
    fi
    
    # ---------------------- Ensure that is owner of file is root:root -----------------------
    sudo chown root: "${SUDOER_FOLDER}/${SUDOER_FILE}"
    RETURN_CODE=$?

    # If something going wrong -> terminated
    if [ $RETURN_CODE != 0 ]
    then
        sendErrorMsg "Impossible de changer le propriétaire du fichier ${PATH_TO_SUDOER_FILE}"
        sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
        exit $RETURN_CODE
    fi

    # ------------------------ Ensure that the file is read-only (440) ------------------------
    sudo chmod 440 "${SUDOER_FOLDER}/${SUDOER_FILE}"
    RETURN_CODE=$?

    # If something going wrong -> terminated
    if [ $RETURN_CODE != 0 ]
    then
        sendErrorMsg "Impossible de modifier les permissions du fichier ${PATH_TO_SUDOER_FILE}"
        sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
        exit $RETURN_CODE
    fi

}