# Version 1.0 - 2021-10-09 - Maxime Collin - Creation du script
#
# Ce script créé un utilisateur membre du groupe de log et créé son répertoire de log dédié
# Le groupe de log et son répertoire doivent être créé
# 
# Les paramètres possibles sont : 
# --user_name=<username> [requis]
#   L'utilisateur <username> sera créé
# 
# --homeroot=</path/to/home/root> (optionel, par défaut /home)
#   Si définit le home de l'utilisateur sera dans le chemin définit
#   Ex : --homeroot=/data créera le home dans /data/username
# 
# --subLogFolder=<relative/path/to/log/folder> (optionel, par défaut username)
#   Si définit, le répertoire de log sera dans le chemin définit
#   Ex : --subLogFolder=crontab créera le répertoire crontab dans le dossier de log (voir groupe de log)
# 
# --autologin (optionel, par défaut False)
#   Si définit, l'utilisateur sera automatiquement connecté au démarrage
# 


################################################################################################################
################################################# IMPORT MODULES ###############################################
################################################################################################################

import subprocess   # for call unix command groupadd
import pwd          # for get user uid
import grp          # for search if group exist and get back is gid
import os           # for mkdir, chow and chmod function
import sys          # for uncaught exceptions
from Util import util   # A custom lib for check if script is execute with sudoer right and create dir
from Util import log    # A custom lib for get log group name and his root folder


################################################################################################################
################################################# GET PARAMATERS ###############################################
################################################################################################################

# Get user name, if not defined -> terminated
user_name = util.getScriptParameter("user_name=", None)
if user_name is None:
    print ("Erreur : le nom d'utilisateur n'a pas été précisé")
    print ("Fin du script")
    exit(1)

# Get Home root, by default -> /home 
home_root = util.getScriptParameter("homeroot=", "/home")
# Define his homedir
home_user = home_root + "/" + user_name

# Get his sub log folder if defined, by default is user name in root log folder
logFolder = log.Log.ROOTFOLDER + '/' + util.getScriptParameter("subLogFolder=", user_name)

# Activate auto login 
autoLogin = util.getScriptParameter("autoLogin", False)

# Get user's groups
user_groupList = log.Log.LOGGROUP
user_groupList = util.getScriptParameter("groups=", "")
if len(user_groupList) > 0:
    user_groupList += ',' + user_groupList


################################################################################################################
################################################ DEFINE FUNCTIONS ##############################################
################################################################################################################

# -------------------------------------------------------------------------------------------------
# ------------------------------- checkGroupAndLogRootFolderExist() -------------------------------
# -------------------------------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction va vérifier que le groupe de LOG et le répertoire racine des logs existent
#   S'ils n'existent pas, elle quitte le programme avec une erreur
# 
# PARAMÈTRES :
#   Aucun
# 
# RETURN :
#   exit code 1 if FAIL
# 
# USAGE :
#   checkGroupAndLogRootFolderExist()
# 
def checkGroupAndLogRootFolderExist():
    # grp.getgrnam() can raise a KeyError if group doesn't exist
    try:
        # Seach if group exist
        grp.getgrnam(log.Log.LOGGROUP)

    # Group doesn't exist -> terminated
    except KeyError:
        print ("Erreur : le groupe " + log.Log.LOGGROUP + " n'existe pas")
        print ("Fin du script")
        exit(1)

    # Group exist -> continue
    else:
        # Dir Log doesn't exist -> terminated
        if not os.path.isdir(log.Log.ROOTFOLDER):
            print ("Erreur : le répertoire de log du groupe " + log.Log.LOGGROUP + " n'existe pas")
            print ("Fin du script")
            exit(1)
        # Dir Log exist -> continue


# --------------------------------------------------------------------------------------------------------------
# ----------------------------------------- setPermissionsLogUserDir() -----------------------------------------
# --------------------------------------------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction va changer le owner et le groupe du répertoire logFolder
#   pour les attribuer à l'utilisateur créé et son groupe de LOG
# 
# PARAMÈTRES :
#   Aucun
# 
# RETURN :
#   nothing
# 
# USAGE :
#   setPermissionsLogUserDir()
# 
def setPermissionsLogUserDir():
    msg = """Attribution des droits à l'utilisateur {0} et au groupe {1} sur le répertoire {2}"""
    print (msg.format(user_name, log.Log.LOGGROUP, logFolder))

    # Change group of dir to created group. Args are : 
    #   the directory (logFolder)
    #   the uid of new owner (pwd.getpwnam(user_name) for find the user and .pw_uid for getback the uid)
    #   the gid of new group (grp.getgrnam(log.Log.LOGGROUP) for find the group and .gr_gid for getback the gid)
    # Note : grp.getgrnam() can't raise a KeyError because the group exist
    uid = pwd.getpwnam(user_name).pw_uid
    gid = grp.getgrnam(log.Log.LOGGROUP).gr_gid
    os.chown(logFolder, uid, gid)


# --------------------------------------------------------------------------------------
# --------------------------------- enableAutoLogin() ----------------------------------
# --------------------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction active la connexion automatique au démarrage pour l'utilisateur donné
#   Il faut que le script qui la lance utilise les droits sudo
# 
# PARAMÈTRES :
#   user_name:str = le nom de l'utilisateur
# 
# RETURN :
#   nothing
# 
# USAGE :
#   enableAutoLogin('toto')
# 
def enableAutoLogin(user_name):

    print("Activation de la connexion automatique de l'utilisateur : " + user_name)

    # Ckeck if script launch with root permission, if not -> terminated
    if not util.checkIfCurrentIsSUDOER():
        print ("Erreur : Vous n'avez pas les droits pour exécuter ce script")
        print ("Fin du script")
        exit(1)


    # --------------------- Get contents of CONF file ---------------------
    # The conf file
    conf_file_path ="/etc/gdm3/custom.conf"

    # Open the conf file in reading mode
    try:
        conf_file_reading = open(conf_file_path, "r")

    except IOError:
        print ("Erreur : impossible d'ouvrir le fichier " + conf_file_path)
        print ("Fin du script")
        exit(1)

    # Get is contents and close
    conf_contents = conf_file_reading.read()
    conf_file_reading.close()


    # --------------------- Check [daemon] section ----------------------
    # Find the daemon section where put conf
    daemon_str = '[daemon]'
    daemon_len = len(daemon_str)
    daemon_pos = conf_contents.find(daemon_str)
    
    # If daemon section doesn't exist -> terminated
    if daemon_pos == -1:
        print ("Erreur : impossible de trouver la section " + daemon_str)
        print ("Fin du script")
        exit (1)
    
    # Set the begin of daemon section, ie just after [daemon]
    daemon_begin = daemon_pos + daemon_len


    # ---------------------------------- Change CONF -----------------------------------
    # -> set AutomaticLoginEnable to True and uncomment
    autologin_enable_str = 'AutomaticLoginEnable='
    autologin_enable_val = autologin_enable_str + "True"
    autologin_enable_len = len(autologin_enable_val)

    # Looking in conf_contents if AutomaticLoginEnable exist and replace it or insert it
    conf_contents = util.searchAndReplaceTxtOrInsert(
        # add '.*' for regular expression
        '.*' + autologin_enable_str + '.*',
        autologin_enable_val,
        conf_contents,
        # insert it just after daemon section if not find
        daemon_begin
        )

    # -> set AutomaticLogin to user_name
    autologin_user_str = 'AutomaticLogin='
    autologin_user_val = autologin_user_str + user_name

    # Looking in conf_contents if AutomaticLogin exist and replace it or insert it
    conf_contents = util.searchAndReplaceTxtOrInsert(
        # add '.*' for regular expression
        '.*' + autologin_user_str + '.*',
        autologin_user_val,
        conf_contents,
        # insert it just after AutomaticLoginEnable
        daemon_begin + autologin_enable_len + 1
        )


    # ------------------------ Write new CONF file ------------------------
    try:
        conf_file_writing = open(conf_file_path, "w")

    except IOError:
        print ("Erreur : impossible d'ouvrir le fichier " + conf_file_path)
        print ("Fin du script")
        exit(1)

    conf_file_writing.write(conf_contents)
    conf_file_writing.close()


################################################################################################################
###################################################### MAIN ####################################################
################################################################################################################

try:
    # ------------------------------ CHECK PERMISSION -------------------------------

    # Ckeck if script launch with root permission, if not -> terminated
    if not util.checkIfCurrentIsSUDOER():
        print ("Erreur : Vous n'avez pas les droits pour exécuter ce script")
        print ("Fin du script")
        exit(1)
# -> no PermissionError can be raise on the following because we have root permission


    # ----------- CHECK GROUP AND DIR LOG ------------

    # If group or dir log doesn't exists -> terminated
    checkGroupAndLogRootFolderExist()

    # - CREATE DIR HOME ROOT -
    util.createDir(home_root)
    
    # ------------------------------------- CREATE USER -------------------------------------
    try:
        pwd.getpwnam(user_name)
        print("L'utilisateur " + user_name + " existe déjà")
    except KeyError:
        
        try:
            print("Création de l'utilisateur " + user_name)
            # Create user. Args are :
            #   the user_name
            #   -G group_name for add user into his group
            #   -d /path/to/home for specifies home_dir
            #   -m to create home_dir
            subprocess.run(["useradd", user_name, "-G", user_groupList, "-d", home_user, "-m", "-s", "/bin/bash"])
            subprocess.run(["passwd", user_name])
        
        except subprocess.CalledProcessError:
            # useradd in error -> terminated
            print ("Erreur : la création de l'utilisateur " + user_name + " a échouée")
            print ("Fin du script")
            exit(1)

    # ----- CREATE DIR LOG ------
    util.createDir(logFolder)

    # - SET PERMISSION ON DIR - 
    setPermissionsLogUserDir()

    # ------ ACTIVATE AUTO LOGIN ------
    if autoLogin:
        enableAutoLogin(user_name)

except:
    print("Erreur")
    print(sys.exc_info()[0])
    exit(1)