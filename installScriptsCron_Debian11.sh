#!/bin/bash

# Version 1.0 - 2021-10-09 - Maxime Collin - Creation du script
#
# Ce script va installer les scripts à mettre en crontab sur une machine
# Il a besoin des droits admin
# Il va commencer par vérifier que python3 est installé ainsi que git et les installer sinon
# Il créé un groupe et un répertoire pour logguer les scripts si ce n'est pas déjà fait
# Il créé un user pour lancer les scripts et déploi les sources dans son homedir avec git, puis installe les dépendances
#
# Et créé un lien symbolique pour python3 pour l'appeler avec python

source ./Util/util.sh
source ./scripts-cron/Global_VAR.sh

# ------------ RPM Packages needed
echo "Install RPM Packages"
installRPMpackage $UTIL_RPMPACKAGES_NEEDED

# ------------ Python 3 Symlink
echo "Create Symlink for Python3"
createPython3Symlink /usr/bin/python

# ------------ Python Packages needed for install process
echo "Install Python Packages"
installPipPackage --sudoer $UTIL_PIPPACKAGES_NEEDED

# ------------ Put Custom Package in Python PATH
echo "Export Python PATH"
allowENVvarPYTHONPATHforSudoCmd
export PYTHONPATH="${PYTHONPATH}:${PWD}/Util"

# ------------ Create Log Group
echo "Create Log Group and Log Root Folder"
sudo python Util/createLogGroup.py
RETURN_CODE=$?

# If something going wrong -> terminated
if [ $RETURN_CODE != 0 ]
then
    sendErrorMsg "Le groupe n'a pas pu être créé"
    sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
    exit $RETURN_CODE
fi

# ------------ Create User
echo "Create User cron-scripts"
sudo python Util/createUser.py --user_name=$SCRIPTSCRON_USER_NAME --homeroot=$SCRIPTSCRON_USER_HOMEROOT --subLogFolder=$SCRIPTSCRON_USER_SUBLOGFOLDER
RETURN_CODE=$?

# If something going wrong -> terminated
if [ $RETURN_CODE != 0 ]
then
    sendErrorMsg "le user n'a pas pu être créé"
    sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
    exit $RETURN_CODE
fi

echo "Deploying source"
sudo su -m $SCRIPTSCRON_USER_NAME -c 'python -c "from Util import deployGit; deployGit.DeployGit('"'${SCRIPTSCRON_REPO_GITURL}'"')"'

RETURN_CODE=$?

# If something going wrong -> terminated
if [ $RETURN_CODE != 0 ]
then
    sendErrorMsg "les sources n'ont pas pu être déployées"
    sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
    exit $RETURN_CODE
fi

# ------------ Python Packages needed for install process
echo "Install Python Packages"
export SOURCE_REQUIREMENTSFILE="${SCRIPTSCRON_USER_HOMEROOT}/${SCRIPTSCRON_USER_NAME}/$SCRIPTSCRON_REPO_NAME/${SCRIPTSCRON_REPO_REQUIREMENTSFILE}"
su $SCRIPTSCRON_USER_NAME -c "source Util/util.sh; installPipPackage $SOURCE_REQUIREMENTSFILE"
RETURN_CODE=$?

# If something going wrong -> terminated
if [ $RETURN_CODE != 0 ]
then
    sendErrorMsg "les dépendances des sources n'ont pas pu être installées"
    sendErrorMsg "${MSG_END_SCRIPT_ON_ERROR}"
    exit $RETURN_CODE
fi

# ------------ Initialize Crontab
echo "Replace crontab by ${SCRIPTSCRON_CRONFILE} file"
sudo su $SCRIPTSCRON_USER_NAME -c "crontab scripts-cron/${SCRIPTSCRON_CRONFILE}"